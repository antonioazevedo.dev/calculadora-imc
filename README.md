# Calculadora-IMC

## Descrição
Este código é referente a um teste lógico proposto pela CONSISTE. Nele estaremos calculando o Índice de Massa Corporal de um grupo de pessoas cujos dados estão no arquivo DATASET.CSV.

Além disso, os dados calculados serão formatados e armazenados no arquivo de texto: [AntonioCesarAzevedo].txt

## Como usar o código
Para que o código funcione, apenas abra o CMD no diretório no qual o arquio **main.py** esteja presente e digite a seguinte linha de comando:
```
python main.py
```
Nota: é necessário que tenha o arquivo DATASET.CSV presente no mesmo diretório também.

### Contatos
Qualquer dúvida estarei a disposição!
✉️ Email: [antonioazevedo.dev@gmail.com](mailto:antonioazevedo.dev@gmail.com)