# Função que formata uma linha do dataset e a converte para um dicionário.
def formatar_linha(linha):
    linha = linha.split(';')

    # Formatando para float do python
    linha[2] = linha[2].replace(",", ".")
    linha[3] = linha[3].replace(",", ".")
    linha[3] = linha[3].replace("\n", "")

    dado_formatado = {
        'nome': linha[0].replace(" ", "").upper(),
        'sobrenome': ' '.join(linha[1].split()).upper(),
        'peso': float(linha[2]) if linha[2].replace(".", "").isdecimal() else None,
        'altura': float(linha[3]) if linha[3].replace(".", "").isdecimal() else None
    }
    return dado_formatado

# Função que lê o arquivo do dataset e retorna uma lista de dicionários de dados
def get_dados(dataset):
    arq = open(dataset, 'r')
    dados = []
    for indice, linha in enumerate(arq):
        if indice > 0:
            dado = formatar_linha(linha)
            dados.append(dado)
    arq.close()
    return dados

# Função para calcular o IMC
def calcular_imc(peso, altura):
    resultado = '' 

    # Verifica se peso e altura existem
    if peso and altura:
        resultado = peso/(altura**2)
        resultado = round(resultado, 2)
    return resultado

# Função que retorna uma string com todos os dados formatados
def formatar_dados(dados):
    string = ""
    for linha in dados:
        string += linha['nome'] + " "
        string += linha['sobrenome'] + " "
        string += str(calcular_imc(linha["peso"], linha["altura"])).replace(".", ",") + "\n"
    return string

# Função que registra os dados em um arquivo no estilo [meuNomeCompleto].txt
def salvar_dados(dados_formatados):
    arq = open("[AntonioCesarAzevedo].txt", 'w')
    arq.write(dados_formatados)
    arq.close()
    