from functions import get_dados, formatar_dados, salvar_dados

# Obtendo os dados
dados = get_dados('DATASET.CSV')

# Resposta do teste lógico solicitado
print(formatar_dados(dados))
salvar_dados(formatar_dados(dados))